package huoshan

import (
	"encoding/json"
	"fmt"
	"testing"
)

type User struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
	AA   string `json:"aa"`
}

func TestGetExperiment(t *testing.T) {
	config := map[string]interface{}{}

	config = *(GetExperimentConfig("98d6fe6419827e7d84d15fa847102108", "fkx_rule_test_hs01", "userid_01", &config, map[string]interface{}{}))
	fmt.Println(config)
}

func TestGetAllExperiment(t *testing.T) {
	experiment, err := GetAllExperiment("98d6fe6419827e7d84d15fa847102108", "userid_01", map[string]interface{}{"12": "blue"})
	marshal, _ := json.Marshal(experiment)
	fmt.Println(string(marshal), err)

	m := map[int]int{
		1:   6,
		2:   5,
		9:   7,
		6:   6,
		4:   4,
		3:   3,
		8:   8,
		435: 8,
		32:  8,
		44:  8,
	}
	for i, i2 := range m {
		if i2 > 4 {
			delete(m, i)
		}
	}
	fmt.Println(m)
}
