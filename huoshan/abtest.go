package huoshan

import (
	"encoding/json"
	"github.com/lzk97224/datatester-go-sdk/client"
	"github.com/lzk97224/datatester-go-sdk/entities"
	"github.com/lzk97224/datatester-go-sdk/meta/manager"
	"sync"
)

type AbTestClient struct {
	*client.AbClient
	MetaManager *manager.DynamicMetaManager
}

func NewAbTestClient(token string) *AbTestClient {
	var m *manager.DynamicMetaManager
	r := &AbTestClient{
		AbClient: client.NewClient(token, func() (manager.MetaOptionFunc, bool) {
			return func(metaManager *manager.DynamicMetaManager) {
				m = metaManager
			}, true
		}),
		MetaManager: m,
	}
	return r
}

// token获取方式详见接口描述-AbClient
/*
   client.NewClient("appKey"),
   config.WithMetaHost(config.MetaHostCN), // 默认使用国内SAAS域名，私有化需要自行传入产品域名
   config.WithTrackHost(config.TrackHostCN), // 默认使用国内SAAS域名，私有化需要自行传入上报域名
   config.WithWorkerNumOnce(20), // 事件上报协程数，一般不需要设置
   config.WithFetchInterval(60 * time.Second), // meta更新间隔，默认为60s，一般不需要设置
   config.WithAnonymousConfig(true, true), // 匿名上报配置，第一个参数为开启关闭，第二个参数区分saas和私有化
   config.WithLogger(log.NewLogrusAdapt(logrus.New()))) // 自定义日志接口，提供默认实现
*/

var clientMap = make(map[string]*AbTestClient)
var lock sync.Mutex

func abClient(token string) *AbTestClient {
	if r, ok := clientMap[token]; ok && r != nil {
		return r
	} else {
		lock.Lock()
		defer lock.Unlock()
		if r, ok := clientMap[token]; ok && r != nil {
			return r
		} else {
			clientMap[token] = NewAbTestClient(token)
			return clientMap[token]
		}
	}
}

// GetAllExperiment 查询用户所有命中的实验，实验中去掉了不被命中的分组配置
func GetAllExperiment(token string, userId string, attributes map[string]any) ([]entities.Experiment, error) {
	configs, err := abClient(token).GetAllExperimentConfigs(userId, attributes)
	if err != nil {
		return nil, err
	}
	vidList := make([]string, 0)
	for _, v := range configs {
		vidList = append(vidList, v["vid"].(string))
	}

	experiments := getExperimentListByVids(token, vidList)

	return experiments, err
}

// GetExperimentConfig 根据 variantKey 查询用户命中的指定参数配置
func GetExperimentConfig[T any](token, variantKey, userId string, config T, attributes map[string]any) T {

	// attributes: 用户属性
	//attributes := map[string]interface{}{"12": "blue"}
	// decisionId(ssid): 本地分流用户标识，不用于事件上报，请替换为客户的真实用户标识
	// trackId(uuid): 事件上报用户标识，用于事件上报，请替换为客户的真实用户标识
	value, err := abClient(token).Activate(variantKey, userId, userId, config, attributes)
	// 未命中实验和ff的场景下返回固定err且不为空，value返回传入的默认值，可按需使用
	if err != nil {
		return config
	}

	m, ok := value.(map[string]interface{}) // 实验结果
	if !ok {
		return config
	}

	marshal, err := json.Marshal(m)
	if err != nil {
		return config
	}

	err = json.Unmarshal(marshal, &config)
	if err != nil {
		return config
	}

	return config
}

func getExperimentListByVids(token string, vid []string) []entities.Experiment {
	result := make([]entities.Experiment, 0)
	vidMap := make(map[string]bool)
	for _, s := range vid {
		vidMap[s] = true
	}
	for _, experiment := range abClient(token).MetaManager.GetConfig().ExperimentMap {
		var deleteVid []string
		var hitExperiment bool
		for tv, _ := range experiment.VariantMap {
			if vidMap[tv] {
				hitExperiment = true
				result = append(result, experiment)
			} else {
				deleteVid = append(deleteVid, tv)
			}
		}
		if hitExperiment {
			for _, v := range deleteVid {
				delete(experiment.VariantMap, v)
			}
		}
	}
	return result
}
