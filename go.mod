module gitlab.com/wangzhuan/blue-abtest

go 1.18

require github.com/lzk97224/datatester-go-sdk v1.0.5

require (
	github.com/Masterminds/semver/v3 v3.2.0 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/twmb/murmur3 v1.1.5 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
	google.golang.org/protobuf v1.23.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

//replace github.com/lzk97224/datatester-go-sdk v1.0.5 => ../datatester-go-sdk
